jQuery.noConflict();

jQuery(document).ready(function() {

// add a heading for each fields set so we can style it rather than rely on the sketchy legend tag
	jQuery('.survey fieldset').each(function() {
		jQuery(this).before('<h3><span class="tick">&#10004;</span> ' + jQuery('legend', this).html() + '</h3>');
	});

	jQuery('.pagination').append('<span class="notice">');

// close everything except the first accordion item
	jQuery('.survey fieldset').slice(1).slideUp('normal');

// add click link to next section button 
	jQuery('.survey .pagination .b_next').click(function() {
		var oThisFieldset = jQuery(this).closest('fieldset');
		if (checkSectionComplete(oThisFieldset)) {
			oThisFieldset.children('.pagination').children('.notice').html('');
			oThisFieldset.slideUp('normal');
			var oNextFieldset = oThisFieldset.nextAll('fieldset').first();
				oNextFieldset.slideDown('normal', function() {
					jQuery('.survey h3').removeClass('open');
					jQuery(this).prev().addClass('open');
					var offset = jQuery(this).offset();
					jQuery('html, body').animate({
						scrollTop: offset.top - 36,
						scrollLeft: 0
					}, 200);				
				});
				jQuery('.question', oNextFieldset).first().removeClass('disabled');
		} else {
			oThisFieldset.children('.pagination').children('.notice').html('Please complete all questions.');
		}
	});	

// add click link to prev section button 
	jQuery('.survey .pagination .b_prev').click(function() {
		jQuery(this).closest('fieldset').slideUp('normal');
		var oPrevFieldset = jQuery(this).closest('fieldset').prevAll('fieldset').first();
			oPrevFieldset.slideDown('normal', function() {
				var offset = jQuery(this).offset();
				jQuery('html, body').animate({
					scrollTop: offset.top - 36,
					scrollLeft: 0
				}, 200);				
			});		
	});	

// accordion effect	for each fieldset based on it's parent h3
	jQuery('.survey > h3').click(function() {
		if (jQuery(this).next().hasClass('complete')) {
			if (!jQuery(this).hasClass('open')) {
				var bOpenNew = true;
				jQuery(this).prevAll('fieldset').each(function() {
					if (checkSectionComplete(jQuery(this))) {
						console.log('all dood');
					} else {
						console.log('bad piggy');						
						bOpenNew = false;						
						return false;
					}
				});				
				if (bOpenNew) {
					jQuery('fieldset').slideUp('normal');
					jQuery('.survey h3').removeClass('open');
					jQuery(this).addClass('open');
					var oNextFieldset = jQuery(this).nextAll('fieldset').first();
						jQuery('.question', oNextFieldset).first().removeClass('disabled');
						oNextFieldset.slideDown('normal', function() {
							var offset = jQuery(this).offset();
							jQuery('html, body').animate({
								scrollTop: offset.top - 36,
								scrollLeft: 0
							}, 200);				
						});
				}
			}
		}
	});	
	jQuery('.survey h3:first-child').addClass('open');
	
	
// accordion effect	for each fieldset based on it's parent h3
	jQuery('.report > h3').click(function() {
		if (!jQuery(this).hasClass('open')) {
			jQuery('fieldset').slideUp('normal');
			jQuery('.report h3').removeClass('open');
			jQuery(this).addClass('open');
			var oNextFieldset = jQuery(this).nextAll('fieldset').first();
				jQuery('.question', oNextFieldset).first().removeClass('disabled');
				oNextFieldset.slideDown('normal', function() {
					var offset = jQuery(this).offset();
					jQuery('html, body').animate({
						scrollTop: offset.top - 36,
						scrollLeft: 0
					}, 200);				
				});
		}
	});	
	jQuery('.report h3:first-child').addClass('open');

// add a cursor to all the accordion controls	
	jQuery('.report h3').css('cursor', 'pointer');

// add an overlay to disable input
	jQuery('.survey .question').prepend('<div class="overlay">');

// disable everything except the first question
	jQuery('.question').slice(1).addClass('disabled');

// make the next question available on blur of the text box
	jQuery('.survey .question input:text').blur(function() {
		if (jQuery(this).prop('value') != '') {
			jQuery(this).closest('.question').nextAll('.question').first().removeClass('disabled');		
		}
	});

// make the next question available on click of a radio or checkbox
	jQuery('.survey .question input').click(function() {
		if (jQuery(this).prop('type') != 'text') {
			jQuery(this).closest('.question').nextAll('.question').first().removeClass('disabled');
		}
	});

// make the next question available on change of a select
	jQuery('.survey .question select').change(function() {
		jQuery(this).closest('.question').nextAll('.question').first().removeClass('disabled');
	});

// function to count th feedback for each section.
	jQuery('.report fieldset').slice(1).each(function() {
		var iPositive = 0;
		var iNegative = 0;
		var iNeutral = 0;		
		jQuery('.feedback', this).each(function() {		
			if (jQuery(this).hasClass('positive')) {
				iPositive++;				
			}
			if (jQuery(this).hasClass('negative')) {
				iNegative++;				
			}
			if (jQuery(this).hasClass('neutral')) {
				iNeutral++;				
			}
		});

		var sClassToUse = 'neutral';
		if (iPositive > iNegative && iPositive > iNeutral) {
			sClassToUse = 'positive';
		}
		
		if (iNegative > iPositive && iNegative > iNeutral) {
			sClassToUse = 'negative';
		} 
		
		if (iNeutral > iPositive && iNeutral > iNegative) {
			sClassToUse = 'neutral';
		}

		jQuery(this).prevAll('h3').first().addClass(sClassToUse);			

	});


// set up some spans ready for personalisation
	jQuery('label').each(function() {
		var sHTML = jQuery(this).html();
		var sCheck = sHTML.toLowerCase();
		if (sCheck.indexOf('i/we') == 0) {
			sHTML = sHTML.replace(/i\/we/i, '<span class="personal-cap-i">I/we</span>');
			sHTML = sHTML.replace(/i\/we/gi, '<span class="personal-i">I/we</span>');
		} else if (sCheck.indexOf('i/we') > 0) {
			sHTML = sHTML.replace(/i\/we/gi, '<span class="personal-i">I/we</span>');				
		}
		if (sCheck.indexOf('my/our') == 0) {
			sHTML = sHTML.replace(/my\/our/i, '<span class="personal-cap-my">My/our</span>');
			sHTML = sHTML.replace(/my\/our/gi, '<span class="personal-my">My/our</span>');
		} else if (sCheck.indexOf('my/our') > 0) {
			sHTML = sHTML.replace(/my\/our/gi, '<span class="personal-my">My/our</span>');				
		}
		if (sCheck.indexOf('i am/we are') == 0) {
			sHTML = sHTML.replace(/i am\/we are/i, '<span class="personal-cap-am">I am/we are</span>');
			sHTML = sHTML.replace(/i am\/we are/gi, '<span class="personal-am">I am/we are</span>');
		} else if (sCheck.indexOf('i am/we are') > 0) {
			sHTML = sHTML.replace(/i am\/we are/gi, '<span class="personal-am">I am/we are</span>');				
		}
		jQuery(this).html(sHTML);
	});

	jQuery('.question li .optiontext').each(function() {
		var sHTML = jQuery(this).html();
		var sCheck = sHTML.toLowerCase();
		if (sCheck.indexOf('i/we') == 0) {
			sHTML = sHTML.replace(/i\/we/i, '<span class="personal-cap-i">I/we</span>');
			sHTML = sHTML.replace(/i\/we/gi, '<span class="personal-i">I/we</span>');
		} else if (sCheck.indexOf('i/we') > 0) {
			sHTML = sHTML.replace(/i\/we/gi, '<span class="personal-i">I/we</span>');				
		}
		if (sCheck.indexOf('my/our') == 0) {
			sHTML = sHTML.replace(/my\/our/i, '<span class="personal-cap-my">My/our</span>');
			sHTML = sHTML.replace(/my\/our/gi, '<span class="personal-my">My/our</span>');
		} else if (sCheck.indexOf('my/our') > 0) {
			sHTML = sHTML.replace(/my\/our/gi, '<span class="personal-my">My/our</span>');				
		}
		if (sCheck.indexOf('i am/we are') == 0) {
			sHTML = sHTML.replace(/i am\/we are/i, '<span class="personal-cap-am">I am/we are</span>');
			sHTML = sHTML.replace(/i am\/we are/gi, '<span class="personal-am">I am/we are</span>');
		} else if (sCheck.indexOf('i am/we are') > 0) {
			sHTML = sHTML.replace(/i am\/we are/gi, '<span class="personal-am">I am/we are</span>');				
		}
		jQuery(this).html(sHTML);
	});

	jQuery('.report fieldset h3 em').each(function() {
		var sHTML = jQuery(this).html();
		var sCheck = sHTML.toLowerCase();
		console.log(sHTML + ' : ' + sCheck.indexOf('i/we'));
		if (sCheck.indexOf('i/we') == 0) {
			sHTML = sHTML.replace(/i\/we/i, '<span class="personal-cap-i">I/we</span>');
			sHTML = sHTML.replace(/i\/we/gi, '<span class="personal-i">I/we</span>');
		} else if (sCheck.indexOf('i/we') > 0) {
			sHTML = sHTML.replace(/i\/we/gi, '<span class="personal-i">I/we</span>');				
		}
		if (sCheck.indexOf('my/our') == 0) {
			sHTML = sHTML.replace(/my\/our/i, '<span class="personal-cap-my">My/our</span>');
			sHTML = sHTML.replace(/my\/our/gi, '<span class="personal-my">My/our</span>');
		} else if (sCheck.indexOf('my/our') > 0) {
			sHTML = sHTML.replace(/my\/our/gi, '<span class="personal-my">My/our</span>');				
		}
		if (sCheck.indexOf('i am/we are') == 0) {
			sHTML = sHTML.replace(/i am\/we are/i, '<span class="personal-cap-am">I am/we are</span>');
			sHTML = sHTML.replace(/i am\/we are/gi, '<span class="personal-am">I am/we are</span>');
		} else if (sCheck.indexOf('i am/we are') > 0) {
			sHTML = sHTML.replace(/i am\/we are/gi, '<span class="personal-am">I am/we are</span>');				
		}
		jQuery(this).html(sHTML);
	});

	jQuery('.report fieldset h3 span').each(function() {
		var sHTML = jQuery(this).html();
		var sCheck = sHTML.toLowerCase();
		if (sCheck.indexOf('i/we') == 0) {
			sHTML = sHTML.replace(/i\/we/i, '<span class="personal-cap-i">I/we</span>');
			sHTML = sHTML.replace(/i\/we/gi, '<span class="personal-i">I/we</span>');
		} else if (sCheck.indexOf('i/we') > 0) {
			sHTML = sHTML.replace(/i\/we/gi, '<span class="personal-i">I/we</span>');				
		}
		if (sCheck.indexOf('my/our') == 0) {
			sHTML = sHTML.replace(/my\/our/i, '<span class="personal-cap-my">My/our</span>');
			sHTML = sHTML.replace(/my\/our/gi, '<span class="personal-my">My/our</span>');
		} else if (sCheck.indexOf('my/our') > 0) {
			sHTML = sHTML.replace(/my\/our/gi, '<span class="personal-my">My/our</span>');				
		}
		if (sCheck.indexOf('i am/we are') == 0) {
			sHTML = sHTML.replace(/i am\/we are/i, '<span class="personal-cap-am">I am/we are</span>');
			sHTML = sHTML.replace(/i am\/we are/gi, '<span class="personal-am">I am/we are</span>');
		} else if (sCheck.indexOf('i am/we are') > 0) {
			sHTML = sHTML.replace(/i am\/we are/gi, '<span class="personal-am">I am/we are</span>');				
		}
		jQuery(this).html(sHTML);
	});

	jQuery('.survey fieldset h3').each(function() {
		var sHTML = jQuery(this).html();
		var sCheck = sHTML.toLowerCase();
		if (sCheck.indexOf('i/we') == 0) {
			sHTML = sHTML.replace(/i\/we/i, '<span class="personal-cap-i">I/we</span>');
			sHTML = sHTML.replace(/i\/we/gi, '<span class="personal-i">I/we</span>');
		} else if (sCheck.indexOf('i/we') > 0) {
			sHTML = sHTML.replace(/i\/we/gi, '<span class="personal-i">I/we</span>');				
		}
		if (sCheck.indexOf('my/our') == 0) {
			sHTML = sHTML.replace(/my\/our/i, '<span class="personal-cap-my">My/our</span>');
			sHTML = sHTML.replace(/my\/our/gi, '<span class="personal-my">My/our</span>');
		} else if (sCheck.indexOf('my/our') > 0) {
			sHTML = sHTML.replace(/my\/our/gi, '<span class="personal-my">My/our</span>');				
		}
		if (sCheck.indexOf('i am/we are') == 0) {
			sHTML = sHTML.replace(/i am\/we are/i, '<span class="personal-cap-am">I am/we are</span>');
			sHTML = sHTML.replace(/i am\/we are/gi, '<span class="personal-am">I am/we are</span>');
		} else if (sCheck.indexOf('i am/we are') > 0) {
			sHTML = sHTML.replace(/i am\/we are/gi, '<span class="personal-am">I am/we are</span>');				
		}
		jQuery(this).html(sHTML);
	});

// run some javascript based very specifically on this original survey
	surveySpecifics();
});

function surveySpecifics() {

// change stuff based on there answer to the first question
	jQuery('input#q_0_0').click(function() {
		if (jQuery(this).val() == 'An individual') {
			jQuery('.personal-i').html('I');
			jQuery('.personal-my').html('my');			
			jQuery('.personal-am').html('I am');			

			jQuery('.personal-cap-i').html('I');
			jQuery('.personal-cap-my').html('My');			
			jQuery('.personal-cap-am').html('I am');			

			jQuery('.hiddeninputs').append(jQuery('#qContainer_0_2')).hide();
		}

		if (jQuery(this).val() == 'A couple') {
			jQuery('.personal-i').html('we');
			jQuery('.personal-my').html('our');	
			jQuery('.personal-am').html('we are');	

			jQuery('.personal-cap-i').html('We');
			jQuery('.personal-cap-my').html('Our');	
			jQuery('.personal-cap-am').html('We are');	

			jQuery('#qContainer_0_1').after(jQuery('.hiddeninputs #qContainer_0_2'));
			jQuery('#qContainer_0_2').show();
					
		}
	});
	

	var oA_0_0 = jQuery('input#a_0_0');
	if (oA_0_0.length) {
		if (oA_0_0.val() == 'An individual') {
			jQuery('.personal-i').html('I');
			jQuery('.personal-my').html('my');			

			jQuery('.personal-cap-i').html('I');
			jQuery('.personal-cap-my').html('My');
		}

		if (oA_0_0.val() == 'A couple') {
			jQuery('.personal-i').html('we');
			jQuery('.personal-my').html('our');	

			jQuery('.personal-cap-i').html('We');
			jQuery('.personal-cap-my').html('Our');	
		}		
	}

// if they answered no to question about mortgages turn on/off the next question
	jQuery('#qContainer_2_0 input').change(function() {
		var bChecked = false;
		var iChecked = 0;
		jQuery('#qContainer_2_0 input').each(function() {	
			if (jQuery(this).prop('checked')) {
				iChecked++;
				if (jQuery(this).val().toLowerCase().indexOf('mortgage') >= 0)	{
					bChecked = true;
				}
			}
		});
		if (iChecked > 0) {
			if (bChecked) {
				jQuery('#qContainer_2_0').after(jQuery('.hiddeninputs #qContainer_2_1'));
				jQuery('#qContainer_2_1').show();
			} else {
				jQuery('.hiddeninputs').append(jQuery('#qContainer_2_1')).hide();	
//				jQuery('#qContainer_2_1').find('input').prop('checked', false).end().buttonset('refresh');
				jQuery('#qContainer_2_1').find('input').prop('checked', false).end();
				jQuery('#qContainer_2_2').removeClass('disabled');
			}
		}
	});
	
// if they answered no to question about creditcards turn on/off the next question
	jQuery('#qContainer_2_2 input').change(function() {
		var bChecked = false;
		var iChecked = 0;
		jQuery('#qContainer_2_2 input').each(function() {	
			if (jQuery(this).prop('checked')) {
				iChecked++;
				if (jQuery(this).val().toLowerCase().indexOf('yes') >= 0)	{
					bChecked = true;
				}
			}
		});
		if (iChecked > 0) {
			if (bChecked) {
				jQuery('#qContainer_2_2').after(jQuery('.hiddeninputs #qContainer_2_3'));
				jQuery('#qContainer_2_3').show();
			} else {
				jQuery('.hiddeninputs').append(jQuery('#qContainer_2_3')).hide();	
//				jQuery('#qContainer_2_3').find('input').prop('checked', false).end().buttonset('refresh');
				jQuery('#qContainer_2_3').find('input').prop('checked', false).end();
				jQuery('#qContainer_2_4').removeClass('disabled');						
			}
		}
	});	
	
// if they answered no to question about loans turn on/off the next question
	jQuery('#qContainer_2_4 input').change(function() {
		var bChecked = false;
		var iChecked = 0;
		jQuery('#qContainer_2_4 input').each(function() {	
			if (jQuery(this).prop('checked')) {
				iChecked++;
				if (jQuery(this).val().toLowerCase().indexOf('no') < 0)	{
					bChecked = true;
				}
			}
		});
		if (iChecked > 0) {
			if (bChecked) {
				jQuery('#qContainer_2_4').after(jQuery('.hiddeninputs #qContainer_2_5'));
				jQuery('#qContainer_2_5').show();
			} else {
				jQuery('.hiddeninputs').append(jQuery('#qContainer_2_5')).hide();
//				jQuery('#qContainer_2_5').find('input').prop('checked', false).end().buttonset('refresh');
				jQuery('#qContainer_2_5').find('input').prop('checked', false).end();
				jQuery('#qContainer_2_6').removeClass('disabled');
				jQuery(this).closest('.question').nextAll('.pagination').first().removeClass('disabled');							
			}
		}
	});	
		

// if they answered already to question about retirement turn on/off the next questions
	jQuery('#qContainer_5_0 input').change(function() {
		var bChecked = false;
		var iChecked = 0;
		jQuery('#qContainer_5_0 input').each(function() {	
			if (jQuery(this).prop('checked')) {
				iChecked++;
				if (jQuery(this).val().toLowerCase().indexOf('already') < 0)	{
					bChecked = true;
				}
			}
		});
		if (iChecked > 0) {
			if (bChecked) {
				jQuery('#qContainer_5_1').after(jQuery('.hiddeninputs #qContainer_5_2'));
				jQuery('#qContainer_5_2').show();

				jQuery('#qContainer_5_2').after(jQuery('.hiddeninputs #qContainer_5_3'));
				jQuery('#qContainer_5_3').show();

				jQuery('#qContainer_5_3').after(jQuery('.hiddeninputs #qContainer_5_4'));
				jQuery('#qContainer_5_4').show();

			} else {
				jQuery('.hiddeninputs').append(jQuery('#qContainer_5_2')).hide();
				jQuery('.hiddeninputs').append(jQuery('#qContainer_5_3')).hide();
				jQuery('.hiddeninputs').append(jQuery('#qContainer_5_4')).hide();
//				jQuery('#qContainer_5_4').find('input').prop('checked', false).end().buttonset('refresh');
				jQuery('#qContainer_5_4').find('input').prop('checked', false).end();
				jQuery('#qContainer_5_5').removeClass('disabled');			
			}
		}
	});	
		
	
}

function checkSectionComplete(oSection) {
	var isComplete = true;
	jQuery('.question', oSection).each(function() {
		var oQuestion = jQuery(this);
		var iChecked = 0;
		if (oQuestion.hasClass('isradio')) {
			jQuery('input', oQuestion).each(function() {
				if (jQuery(this).prop('checked')) {
					iChecked++;
				}
			});
			if (iChecked == 0) {
				isComplete = false;
			}
		}
		if (oQuestion.hasClass('ischeckbox')) {
			jQuery('input', oQuestion).each(function() {
				if (jQuery(this).prop('checked')) {
					iChecked++;
				}
			});
			if (iChecked == 0) {
				isComplete = false;
			}
		}
		if (oQuestion.hasClass('isselect')) {
			if(isEmpty(jQuery('select', oQuestion).val()))  {
				isComplete = false;
			}
		}
		if (oQuestion.hasClass('istext')) {
			if(isEmpty(jQuery('input', oQuestion).val()))  {
				isComplete = false;
			}
		}		
	});
	if (isComplete) {
		oSection.prev().addClass('complete');
		oSection.addClass('complete');
	} else {
		oSection.prev().removeClass('complete');
		oSection.removeClass('complete');
		oSection.nextAll().removeClass('complete');
	}
	return isComplete;
}

// function to check if the form has been completed properly
function checkComplete(oForm) {
	var sAlert = '';	
	jQuery('.survey input:text').each(function() {
		console.log('one');
		sValidateAs = jQuery(this).attr('validateas');
		switch (sValidateAs) {
		case 'text': 
			if (isEmpty(jQuery(this).val())) {
				sAlert = sAlert + ' • ' + jQuery(this).prev('label').html() + '\n';
			}
			break;
		case 'phone': 
			jQuery(this).val(reduceToDigits(jQuery(this).val()));
			if (jQuery(this).val().length < 8) {
				sAlert = sAlert + ' • ' + jQuery(this).prev('label').html() + '\n';
			}
			break;
		case 'email': 
			if (!isValidEmail(jQuery(this).val())) {
				sAlert = sAlert + ' • ' + jQuery(this).prev('label').html() + '\n';
			}
			break;
		case 'number': 
			jQuery(this).val(reduceToDigits(jQuery(this).val()));
			if (isEmpty(jQuery(this).val())) {
				sAlert = sAlert + ' • ' + jQuery(this).prev('label').html() + '\n';
			}
			break;
		};			
	});
	if (sAlert) {
		alert ("Your form is incomplete.\n" +
			   "Please supply valid information for:\n" +
				sAlert);
		return false;
	} else { 
		return true;
	}

}	

// function to reduce a value to digits only
function reduceToDigits(strValue) {
	return (strValue.replace (/([^0-9])/g, "", strValue));
}

// function to check it a string is empty
function isEmpty(strValue) {
	return (! strValue.replace (/^(\s*)/, "", strValue));
}

// function to check a string is a valid email address
function isValidEmail(emailStr) {

		var emailPat=/^(.+)@(.+)$/; 
		var specialChars="\\(\\)<>@,;:\\\\\\\"\\.\\[\\]";
		var validChars="\[^\\s" + specialChars + "\]"; 
		var quotedUser="(\"[^\"]*\")";
		var ipDomainPat=/^\[(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})\]$/;
		var atom=validChars + '+'; 
		var word="(" + atom + "|" + quotedUser + ")";
		var userPat=new RegExp("^" + word + "(\\." + word + ")*$");
		var domainPat=new RegExp("^" + atom + "(\\." + atom +")*$");

		var matchArray=emailStr.match(emailPat)
		if (matchArray==null) {
			return false;
		}
		
		var user=matchArray[1];
		var domain=matchArray[2];
		
		if (user.match(userPat)==null) {
			return false;
		}
		
		var IPArray=domain.match(ipDomainPat)
		if (IPArray!=null) {
			for (var i=1;i<=4;i++) {
				if (IPArray[i]>255) {
					return false;
				}
			}
			return true;
		}
		
		var domainArray=domain.match(domainPat)
		if (domainArray==null) {
		    return false;
		}

		var atomPat=new RegExp(atom,"g")
		var domArr=domain.match(atomPat)
		var len=domArr.length
		if (domArr[domArr.length-1].length<2 || 
			domArr[domArr.length-1].length>4) {
			return false;
		}
	
		if (len<2) {
			return false;
		}

		return true;
	}
 